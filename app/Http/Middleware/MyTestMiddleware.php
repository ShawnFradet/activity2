<?php

namespace App\Http\Middleware;

use App\Services\Utility\MyLogger2;
use Closure;
use Illuminate\Support\Facades\Cache;

class MyTestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        MyLogger2::info("MyTestMiddlewareCalled");
        if ($request->input('username'))
        {
            Cache::put('mydata', $request->input('username'), $seconds = 60);
            MyLogger2::info("Data added to cache");
        }
        else
        {
            $password = Cache::get('mydata');
            if($password)
            {
                MyLogger2::info("Valid Data retrieved from cache");
            }
            else
            {
                MyLogger2::info("No Data retrieved from cache");
            }
        }
        return $next($request);
    }
}
