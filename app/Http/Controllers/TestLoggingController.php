<?php

namespace App\Http\Controllers;

use App\Services\Utility\ILoggerService;
use Illuminate\Http\Request;

class TestLoggingController extends Controller
{
    protected $logger;

    public function __construct(ILoggerService $iloggerService)
    {
        $this->logger = $iloggerService;
    }

    public function index()
    {
        $this->logger->info("This is the IoC logger for activity 5");
    }
}
