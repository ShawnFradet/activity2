<?php

namespace App\Http\Controllers;

use App\Models\DTO;
use App\Services\Business\SecurityService;

class UsersRestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $securityService = new SecurityService();

        $dto = new DTO(0, "Success", $securityService->getAllUsers());

        return json_encode($dto);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $securityService = new SecurityService();

        $user = $securityService->getUser($id);

        if ($user)
        {
            $dto = new DTO(0, "Success", $securityService->getUser($id));
        }
        else
        {
            $dto = new DTO(1, "User does not exist", $securityService->getUser($id));
        }

        return json_encode($dto);
    }

}
