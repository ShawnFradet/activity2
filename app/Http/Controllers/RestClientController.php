<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;

class RestClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $serviceURL = "http://localhost/CST/Activity2";
        $api = "Activity2/usersrest";
        $param = "";
        $uri = $api . "/" . $param;

        try
        {
            $client = new Client(['base_uri' => $serviceURL]);
            $response = $client->request('GET', $uri);

            if($response->getStatusCode() == 200)
                return $response->getBody();
            else
                return "There was an error: " . $response->getStatusCode();
        }
        catch (ClientException $e)
        {
            return "There was an exception: " . $e->getMessage();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $serviceURL = "http://localhost/CST/Activity2";
        $api = "Activity2/usersrest";
        $param = $id;
        $uri = $api . "/" . $param;

        try
        {
            $client = new Client(['base_uri' => $serviceURL]);
            $response = $client->request('GET', $uri);

            if($response->getStatusCode() == 200)
                return $response->getBody();
            else
                return "There was an error: " . $response->getStatusCode();
        }
        catch (ClientException $e)
        {
            return "There was an exception: " . $e->getMessage();
        }
    }

}


