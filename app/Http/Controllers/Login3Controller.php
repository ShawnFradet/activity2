<?php

namespace App\Http\Controllers;

use App\Services\Utility\MyLogger2;
use Illuminate\Http\Request;
use App\Services\Business\SecurityService;
use App\Models\UserModel;

class Login3Controller extends Controller
{
    public function index(Request $request)
    {
        $this->validateForm($request);

        //MyLogger1::info("Entering LoginController::index()");
        MyLogger2::info("Entering LoginController::index()");

        $username = $request->input('username');
        $password = $request->input('password');

        //MyLogger1::info("Parameters are: ", array("username"=>$username, "password"=>$password));
        MyLogger2::info("Parameters are: ", array("username"=>$username, "password"=>$password));

        $userModel = new UserModel($username, $password);

        $securityService = new SecurityService();

        $result = $securityService->login($userModel);

        if($result)
        {
            //MyLogger1::info("Exit LoginController::index() with Login Passed");
            MyLogger2::info("Exiting LoginController::index() with Login Passed");
            return view('loginPassed', compact('userModel'));
        }
        else {
            //MyLogger1::info("Exit LoginController::index() with Login Failed");
            MyLogger2::info("Exiting LoginController::index() with Login Failed");
            return view('loginFailed', compact('userModel'));
        }
    }

    private function  validateForm(Request $request){

        $rules  = ['username'=>'Required | Between: 4,15 |Alpha', 'password'=>'Required|Between:4,10'];

        $this->validate($request, $rules);
    }
}
