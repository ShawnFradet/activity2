<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Business\OrderService;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $product = $request->input('product');
        
        $orderService = new OrderService();
        
        $orderService->createOrder($firstname, $lastname, $product);        
    }
}
