<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\Business\SecurityService;
use App\Models\UserModel;

class LoginController extends Controller
{
    public function index(Request $request)
    {      
        Log::info("Entering LoginController::index()");
        
        $username = $request->input('username');
        $password = $request->input('password');
        
        Log::info("Parameters are: ", array("username"=>$username, "password"=>$password));
                
        $userModel = new UserModel($username, $password);
        
        $securityService = new SecurityService();
        
        $result = $securityService->login($userModel);
        
        return view('loginPassed2', compact('userModel'));
    }
}
