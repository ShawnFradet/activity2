<?php
namespace App\Services\Data;

class OrderDAO
{
    private $connection;
    
    public function __construct($connection)
    {
        $this->connection = $connection;
    }
    
    public function addOrder($product, $customerID)
    {
        // Prepare SQL String
        $sql_query = "INSERT INTO orders (PRODUCT, CUSTOMER_ID) VALUES (?, ?)";
        $stmt = $this->connection->prepare($sql_query);
        
        $stmt->bind_param("si", $product, $customerID);
        
        // Execute statement return boolean.
        if ($stmt->execute())
        {
            return $stmt->insert_id;
        }
        else {
            return false;
        }
    }
}

