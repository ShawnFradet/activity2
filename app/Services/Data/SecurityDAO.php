<?php
namespace App\Services\Data;
use App\Services\Utility\MyLogger2;
use mysqli;
use App\Models\UserModel;
use Exception;

class SecurityDAO
{
    public function findAllUsers()
    {
        $conn = new mysqli('localhost', 'root', 'root', 'activity2');

        // Prepare search string
        $sql_query = "SELECT * FROM users";
        $stmt = $conn->prepare($sql_query);

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows == 0) {

            return null;
        }
        else
        {
            // Array to hold results
            $user_array = array();

            // Step through results and create new UserModel
            while ($user = $result->fetch_assoc())
            {
                $returnedUser = new UserModel($user["USERNAME"], $user["PASSWORD"]);

                array_push($user_array, $returnedUser);
            }

            return $user_array;
        }
    }

    public function findUserByID($id)
    {
        $conn = new mysqli('localhost', 'root', 'root', 'activity2');

        // Prepare search string
        $sql_query = "SELECT * FROM users WHERE ID = ?";
        $stmt = $conn->prepare($sql_query);

        $stmt->bind_param("i", $id);

        $stmt->execute();

        $result = $stmt->get_result();

        if ($result->num_rows == 0) {

            return null;
        }
        else
        {
            $user = $result->fetch_assoc();

            $returnedUser = new UserModel($user["USERNAME"], $user["PASSWORD"]);

            return $returnedUser;
        }
    }

    public function findByUser(UserModel $userModel)
    {
        //MyLogger1::info("Entering SecurityDAO::findByUser()");
        MyLogger2::info("Entering SecurityDAO::findByUser()");
        $conn = new mysqli('localhost', 'root', 'root', 'activity2');

        $username = $userModel->getUsername();
        $password = $userModel->getPassword();

        //MyLogger1::info("SecurityDAO::findByUser() arguments: ", array('username'=>$username, 'password'=>$password));
        MyLogger2::info("SecurityDAO::findByUser() arguments: ", array('username'=>$username, 'password'=>$password));

        // Prepare search string
        $sql_query = "SELECT * FROM users WHERE USERNAME LIKE ? AND PASSWORD LIKE ?";
        $stmt = $conn->prepare($sql_query);
        $stmt->bind_param("ss", $username, $password);

        // Execute statement and get results.
        try {
            $stmt->execute();
        }
        catch (Exception $e)
        {
            //MyLogger1::error($e);
            MyLogger2::error($e);
        }


        $result = $stmt->get_result();

        // Set results to array. If no results return null.
        if ($result->num_rows == 0)
        {
            //MyLogger1::info("Exiting SecurityDAO::findByUser()");
            MyLogger2::info("Exiting SecurityDAO::findByUser()");
            return false;
        }
        else
        {
            //MyLogger1::info("Exiting SecurityDAO::findByUser()");
            MyLogger2::info("Exiting SecurityDAO::findByUser()");
            return true;
        }
    }
}

