<?php
namespace App\Services\Data;

class CustomerDAO
{
    private $connection;
    
    public function __construct($connection)
    {
        $this->connection = $connection;
    }
    
    public function addCustomer($firstName, $lastName)
    {
        // Prepare SQL String
        $sql_query = "INSERT INTO customer (FIRST_NAME, LAST_NAME) VALUES (?, ?)";
        $stmt = $this->connection->prepare($sql_query);
        
        $stmt->bind_param("ss", $firstName, $lastName);
        
        // Execute statement return boolean.
        if ($stmt->execute())
        {
            return $stmt->insert_id;
        }
        else {
            return false;
        }
    }
}

