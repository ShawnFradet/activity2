<?php
namespace App\Services\Business;
use App\Services\Data\OrderDAO;
use App\Services\Data\CustomerDAO;
use mysqli;

class OrderService
{
    public function createOrder($firstName, $lastName, $product)
    {
        $connection = new mysqli('localhost', 'root', 'root', 'activity3');
        
        $connection->autocommit(FALSE);
        $connection->begin_transaction();
        
        $customerDAO = new CustomerDAO($connection);
        $orderDAO = new OrderDAO($connection);
        
        $customerID = $customerDAO->addCustomer($firstName, $lastName);
        
        $orderID = $orderDAO->addOrder($product, $customerID);
        
        if($orderID)
        {
            $connection->commit();
        }
        else
        {
            $connection->rollback();
        }
        
    }
}

