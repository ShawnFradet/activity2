<?php
namespace App\Services\Business;

use App\Services\Data\SecurityDAO;
use App\Models\UserModel;
use App\Services\Utility\MyLogger2;
use Exception;

class SecurityService
{
    public function login(UserModel $userModel)
    {
        //MyLogger1::info("Entering SecurityService::login()");
        MyLogger2::info("Entering SecurityService::login()");
        $securityDAO = new SecurityDAO();
        $result = null;

        try{
            $result = $securityDAO->findByUser($userModel);
        }
        catch(Exception $e)
        {
            //MyLogger1::error($e);
            MyLogger2::error($e);
        }

        //MyLogger1::info("Exiting SecurityService::login()");
        MyLogger2::info("Exiting SecurityService::login()");

        return $result;
    }

    public function getAllUsers()
    {
        $securityDAO = new SecurityDAO();

        return $securityDAO->findAllUsers();
    }

    public function getUser($id)
    {
        $securityDAO = new SecurityDAO();

        return $securityDAO->findUserByID($id);
    }
}

