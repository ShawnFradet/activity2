<?php


namespace App\Services\Utility;


interface ILoggerService
{
    public function debug($message, $array=null);

    public function info($message, $array=null);

    public function warning($message, $array=null);

    public function error($message, $array=null);
}
