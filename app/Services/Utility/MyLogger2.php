<?php

namespace App\Services\Utility;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MyLogger2 implements ILogger
{
    private static $instance = null;

    private static function getLogger()
    {
        if(is_null(self::$instance))
        {
            $logger = new Logger('testLogger');
            $stream = new StreamHandler(__DIR__ . '/../../../storage/logs/myapp.log', Logger::DEBUG);
            $stream->setFormatter(new LineFormatter());
            $logger->pushHandler($stream);
            self::$instance = $logger;
        }

        return self::$instance;
    }

    public static function debug($message, $array = null)
    {
        if($array != null)
        {
            self::getLogger()->addDebug($message, $array);
        }
        else
        {
            self::getLogger()->addDebug($message);
        }
    }

    public static function info($message, $array = null)
    {
        if($array != null)
        {
            self::getLogger()->addInfo($message, $array);
        }
        else
        {
            self::getLogger()->addInfo($message);
        }

    }

    public static function warning($message, $array = null)
    {
        if($array != null)
    {
        self::getLogger()->addWarning($message, $array);
    }
    else
    {
        self::getLogger()->addWarning($message);
    }
    }

    public static function error($message, $array = null)
    {
        if($array != null)
        {
            self::getLogger()->addError($message, $array);
        }
        else
        {
            self::getLogger()->addError($message);
        }
    }
}
