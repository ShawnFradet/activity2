<?php


namespace App\Services\Utility;


interface Ilogger
{
    public static function debug($message, $array=null);

    public static function info($message, $array=null);

    public static function warning($message, $array=null);

    public static function error($message, $array=null);
}
