<?php

namespace App\Services\Utility;

use Illuminate\Support\Facades\Log;

class MyLogger3 implements ILoggerService
{
    public function debug($message, $array=null)
    {
        if($array != null)
        {
            Log::debug($message, $array);
        }
        else
        {
            Log::debug($message);
        }

    }

    public function info($message, $array=null)
    {
        if($array != null)
        {
            Log::info($message, $array);
        }
        else
        {
            Log::info($message);
        }
    }

    public function warning($message, $array=null)
    {
        if($array != null)
        {
            Log::warning($message, $array);
        }
        else
        {
            Log::warning($message);
        }
    }

    public function error($message, $array=null)
    {
        if($array != null)
        {
            Log::error($message, $array);
        }
        else
        {
            Log::error($message);
        }
    }
}
