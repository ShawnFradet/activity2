@extends('layouts.appmaster')
@section('title', 'Login Page')
@section('content')
<form action="dologin3" method="POST">
	<input type="hidden" name="_token"
		value="<?php echo csrf_token() ?>">
	<h2>Login</h2>
	<div>
	<label for="username">Username:</label>
	<input type="text" name="username" />
	<h5><?php echo $errors->first('username')?></h5>
	</div>
	</br>
	<div>
	<label for="password">Password:</label>
	<input type="text" name="password" />
	<h5><?php echo $errors->first('password')?></h5>	
	</div>
	<div>	
	<button type="submit" value="">Submit</button>
	</div>	
</form>
@endsection