<div align='center'>
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
        <div class="container"><a class="navbar-brand" href="#">Welcome to Activity 5&nbsp;</a><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="http://www.gcu.edu" target="_blank">GCU</a></li>
                    <li class="nav-item"></li>
                </ul><span class="navbar-text actions"> <a class="login" href="login5">Log In</a><a class="btn btn-light action-button" role="button" href="/">Logout</a></span>
            </div>
        </div>
    </nav>
</div>
